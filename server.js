var express = require('express');
var app = express();
var port = 8489;

app.get('/', function(req, res){
	res.send('Hola Bienvenido, este es un servidor NodeJS - Express...!');
});

app.get('/prueba', function(req, res){
	res.send('Probando otra URL con un método del servidor.');
});

app.listen(port);

console.log('Servidor desarrollado en express corriendo en el puerto ' + port);
